package chap3;

/**
 * this is a program to demonstrate the usage of constructor
 */
public class Computer {
    // properties / fields / variable
    double cpuSpeed; // in Ghz

    // constructor - method name equal to class name
    // to initialize the properties
    Computer() {
        cpuSpeed = 3.4;
        System.out.println("im in constructor");
    }

    // this is also constructor
    Computer(double speed) {
        cpuSpeed = speed;
    }

    void setCpuSpeed(double cpuSpeed) {
        this.cpuSpeed = cpuSpeed;
    }

    public static void main(String[] args) {
        Computer comp1 = new Computer(); // instantiate an object
        comp1.cpuSpeed = 2.5;
        System.out.println("Comp1 CPU Speed = " + comp1.cpuSpeed);

        Computer comp2 = new Computer();
        comp2.setCpuSpeed(4.0);
        System.out.println("Comp2 CPU Speed = " + comp2.cpuSpeed);

        Computer comp3 = new Computer(8.0);
        System.out.println("Comp3 CPU Speed = " + comp3.cpuSpeed);
    }
}
