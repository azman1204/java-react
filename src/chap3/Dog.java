package chap3;

public class Dog extends Animal {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.species = "Bulldog";
        System.out.println("Dog species = " + dog.species);
    }
}
