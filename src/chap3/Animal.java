package chap3;

public class Animal {
    int legs;
    String name;
    String species;

    public String getName() {
        return name;
    }


    // final here means cannot override
    // deprecated means to remove in next release
    @Deprecated
    final public int getLegs() {
        return legs;
    }
}
