package chap3;

public class WordTool {
    public static void testing() {
        System.out.println("Testing..");
    }

    public static void main(String[] args) {
        // static is something(variable  method) belongs to class, not to object
        testing();
        WordTool.testing();

        WordTool wc = new WordTool();
        int count = wc.wordCount("testing one two tree");
        System.out.println("word count = " + count);
    }

    public int wordCount(String s) {
        int count = 0;
        if (!(s == null || s.isEmpty())) {
            String[] w = s.split("\\s+");
            count = w.length;
        }
        return count;
    }
}
