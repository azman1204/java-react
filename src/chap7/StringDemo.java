package chap7;

import java.util.Locale;

public class StringDemo {
    public static void main(String[] args) {
        // string is immutable - cannot change
        String str = "my name is azman";
        String str2 = str.toUpperCase();
        String str3 = str.substring(11);
        System.out.printf("str = %s , str2= %s, str3 = %s \n", str, str2, str3);

        // concat
        String s1 = "my name ";
        String s2 = "azman";
        String s3 = s1.concat(s2);
        System.out.println(s3);

        // search
        String sentence = "Hello Java Reader, how are you";
        boolean isAvail = sentence.toLowerCase().contains("java");
        if (isAvail)
            System.out.println("Yes");
        else
            System.out.println("No");

        // replace
        String s4 = sentence.replace("Java", "Jawa");
        System.out.println(s4);

        // convert string to array
        String[] arr = sentence.split("\\s+");
        for(String word : arr) {
            System.out.println(word);
        }
    }
}
