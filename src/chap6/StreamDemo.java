package chap6;

import java.util.stream.Stream;

public class StreamDemo {
    public static void main(String[] args) {
        // filter - take a subset of data
        Stream.of(1,2,3,4,5,6,7,8,9,10)
                .filter((i) -> i > 3)
                .forEach(System.out::println);

        // map - operate on each data
        Stream.of(1,2,3,4,5,6,7,8,9,10)
                .map((no) -> {
                    return no > 3;
                })
                .forEach((i) -> System.out.println(i));

        // sort
        Stream.of(1,2,3,10,9,7,8,6,4,5)
                .sorted()
                .forEach((i) -> System.out.println(i));

        // Descending order
        Stream.of(1,2,3,10,9,7,8,6,4,5)
                .sorted((no1, no2) -> no2.compareTo(no1))
                .forEach((i) -> System.out.println(i));

        // reduce - aggregate value
        int sum = Stream.of(1,2,3,4,5)
                .reduce(0, (no1, no2) -> no1 + no2);
        System.out.println("Sum = " + sum);
    }
}
