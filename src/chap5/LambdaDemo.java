package chap5;

public class LambdaDemo {
    public static void main(String[] args) {
        Message msg = () -> System.out.println("Hello Lambda");
        msg.printable();

        Alert danger = (name) -> "Danger " + name;
        String txt = danger.alert("John Doe");
        System.out.println(txt);
    }
}

// this is annotation
@FunctionalInterface
interface Message {
    public void printable();
    //public void show();
}

@FunctionalInterface
interface Alert {
    public String alert(String str);
}
