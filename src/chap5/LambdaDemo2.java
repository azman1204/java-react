package chap5;

import java.util.ArrayList;
import java.util.List;

public class LambdaDemo2 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("John Doe");
        list.add("Lee");
        list.add("Abu");
        list.add("Vasantha");
        list.add("Vivian");
        list.forEach((name) -> System.out.println(name));
    }
}
