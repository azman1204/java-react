package chap4;

import java.util.ArrayList;

public class EmployeeDemo {
    public static void main(String[] args) {
        ArrayList<Employee> employees = new ArrayList<Employee>();
        Employee emp1 = new Employee();
        emp1.name = "John Doe";
        emp1.salary = 5000.00f;
        employees.add(emp1);

        Employee emp2 = new Employee();
        emp2.name = "Labu";
        emp2.salary = 6000.00f;
        employees.add(emp2);

        for(Employee emp: employees) {
            System.out.printf("Name = %s, Salary = %f \n", emp.name, emp.salary);
        }
    }
}
