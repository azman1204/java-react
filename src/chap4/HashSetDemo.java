package chap4;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class HashSetDemo {
    public static void main(String[] args) {
        //new HashSetDemo().demo1();
        new HashSetDemo().demo2();
    }

    public void demo1() {
        Integer[] myArray = new Integer[] {3,25,2,79,2};
        Set<Integer> mySet = new HashSet<>(Arrays.asList(myArray));
        mySet.add(10);
        mySet.remove(25);
        //mySet.clear(); // delete all data
        System.out.println(mySet);
    }

    // the value inside HashSet cannot primitive type
    public void demo2() {
        // hashset cannot sort
        // <Generic> - limit the Object type that can go inside a HashSet
        HashSet<Integer> myset = new HashSet<>();
        myset.add(3);
        myset.add(10);
        myset.add(12);
        myset.add(1);
        myset.add(50);
        System.out.println(myset);
    }
}
