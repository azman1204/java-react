package chap4;

import java.util.Scanner;

public class GuessGame {
    public static void main(String[] args) {
        int noTrial = 0;
        int rand = (int)(Math.random() * 100);
        System.out.println(rand);
        while(true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Guess a number : ");
            int guessNo = scanner.nextInt();

            if (guessNo == rand) {
                System.out.printf("You win after %d trial \n", noTrial);
                System.out.println("Do you want to play more (Y/N)");
                String answer = scanner.next();
                if (answer.equals("N")) {
                    break;
                } else {
                    rand = (int)(Math.random() * 100);
                    noTrial = 0;
                }
            } else {
                // not correct
                if (guessNo > rand)
                    System.out.println("Guess lower");
                else
                    System.out.println("Guess Higher");
            }
            noTrial++;
        }
    }
}
