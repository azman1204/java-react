package chap10;

import javax.swing.plaf.nimbus.State;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class JDBCDemo {
    public static void main(String[] args) {
        //new JDBCDemo().findAll();
        //new JDBCDemo().createEmployee();
        //new JDBCDemo().updateEmployee();
        new JDBCDemo().deleteEmployee();
    }

    public Statement getConn() {
        String path = "C:\\Users\\azman\\Desktop\\Java React Training\\tutorial.db";
        Statement stmt = null;
        try {
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + path);
            stmt = conn.createStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return stmt;
    }

    public void findAll() {
        try {
            Statement stmt = getConn();
            String sql = "SELECT * FROM employee";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getString("name"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createEmployee() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a name : ");
        String name = scanner.next();
        System.out.println("Please enter age : ");
        int age = scanner.nextInt();
        try {
            Statement stmt = getConn();
            String sql = "INSERT INTO employee(name, age) VALUES('"+name+"',"+age+" )";
            stmt.execute(sql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // update data
    public void updateEmployee() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a name : ");
        String name = scanner.next();
        System.out.println("Please enter id : ");
        int id = scanner.nextInt();
        try {
            Statement stmt = getConn();
            String sql = "UPDATE employee SET name = '" + name + "' WHERE id = " + id;
            stmt.executeUpdate(sql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void deleteEmployee() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter id : ");
        int id = scanner.nextInt();
        try {
            Statement stmt = getConn();
            String sql = "DELETE FROM employee WHERE id = " + id;
            stmt.execute(sql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
